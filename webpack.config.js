
const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const BundleTracker = require('webpack-bundle-tracker');
const InjectAssetPlugin = require('ripple-replace-wpl-plugin');
const fs = require('fs');

module.exports = env =>  {

    let production = Boolean(env && ('production' in env) && env.production);
    let hot = Boolean(env && ('hot' in env) && env.hot);
    let port = (env && ('port' in env) && env.port) || 3000;
    let host = (env && ('host' in env) && env.host) || "localhost";
    if (production) console.log('**** PRODUCTION BUILD ************');
    let nodepath = null;
    if (env && ('nodepath' in env)) {
        nodepath = env.nodepath;
    }

    let conf = {
        context: __dirname,
        mode: production?'production':'development',
        target: 'web',

        entry: {
            MainApp: ['@babel/polyfill','./frontend/src/main-app', './frontend/src/custom.scss'],
        },

        output: {
            path: path.resolve('./static/bundles/local/'),
            filename: "[name]-[contenthash].js"
        },

        externals: [], // add all vendor libs

        plugins: [
        ],

        module: {
            rules: [
                {
                    test: /\.js.?$/,
                    exclude: /node_modules|ripple-react/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                        ["@babel/plugin-proposal-decorators",{"legacy": true}],
                                        ["@babel/plugin-proposal-class-properties", {"loose": true}],
                                        'babel-plugin-styled-components',
                                        '@babel/plugin-transform-runtime',
                                        "@babel/plugin-syntax-dynamic-import",
                                        ["module:babel-root-slash-import", {"rootPathSuffix": "src"}],
                                        ["react-hot-loader/babel"]
                            ],
                            presets: [['@babel/preset-env', {"modules":false}],"@babel/preset-flow",'@babel/preset-react']
                        }
                    }
                },
                {test: /\.css$/, use: ['style-loader', 'css-loader']},
                {
                    test: /\.(scss)$/,
                    use: [
                        { loader: 'style-loader' }, // inject CSS to page
                        { loader: 'css-loader' }, // translates CSS into CommonJS modules
                        { loader: 'sass-loader'}, // compiles Sass to CSS
                    ]
                }
            ],
        },
        resolve: {
            modules: ['node_modules',],
            extensions: ['.wasm', '.mjs', '.js', '.json', '.jsx'],
            symlinks: false,
            alias: {
            }
        },
        resolveLoader: {
            modules: [ nodepath?nodepath:'node_modules', ],
        }
    };

    if (production) {


        // ----- Production Config -------------

        conf.output.path = path.resolve('./static/bundles/prod/');
        //conf.output.publicPath = '/static/bundles/prod/';

        conf.plugins = conf.plugins.concat([
            new webpack.optimize.AggressiveMergingPlugin(), //Merge chunks
            new webpack.HashedModuleIdsPlugin(),
            new InjectAssetPlugin(__dirname, './templates/', ['main-app',]),
        ]);

        conf.optimization = {
            minimizer: [new TerserPlugin()],
            runtimeChunk: 'single',
            mergeDuplicateChunks: true,
            removeEmptyChunks: true,
            splitChunks: {
              chunks: 'all',
              maxInitialRequests: Infinity,
              minSize: 4096,
              cacheGroups: {
                vendor: {
                  test: /[\\/]node_modules[\\/]/,
                  name(module) {
                    // get the name. E.g. node_modules/packageName/not/this/part.js
                    // or node_modules/packageName
                    const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                    // npm package names are URL-safe, but some servers don't like @ symbols
                    return `npm.${packageName.replace('@', '')}`;
                  },
                },
              },
            },
        };

        // -------------------------------------

    }
    else {

        // ----- Development Config -------------

        conf.plugins = conf.plugins.concat([
            new webpack.HotModuleReplacementPlugin(),
            new BundleTracker({filename: './webpack-stats-local.json'}),

        ]);

        conf.devtool = 'inline-source-map';
        conf.output.publicPath = 'http://'+host+':'+port+'/assets/bundles/';
        conf.output.filename = "[name]-[hash].js"
        conf.devServer = {
            publicPath: 'http://'+host+':'+port+'/assets/bundles/',
            hot: hot,
            port: port,
            host: host,
            headers: {
                "Access-Control-Allow-Origin": "http://"+host+':'+port,
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Headers": "Content-Type, Authorization, x-id, Content-Length, X-Requested-With",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS"
            },
        };


        // -------------------------------------

    }


    return conf;
};
