import React from 'react'
import { observer } from 'mobx-react'
import { Router, Switch, Route } from "react-router-dom"
import CookieBanner from 'react-cookie-banner'
import { ScrollToTop, PageView, history, account, LoginForm } from "ripple-react"
import { withRouter } from 'react-router'
import Loader from 'react-loader'
import { observable } from "mobx"
import ReactGA from 'react-ga'

import { hot } from 'react-hot-loader'
import { EditableText, assetManager} from "ripple-react"


const ScrollToTopWithRouter = withRouter(ScrollToTop);
const PageViewWithRouter = withRouter(PageView);


class Home extends  React.Component {
    render() {
        return (
            <div>
                <div>Hello World!</div>
            </div>
        )
    }
}

class HomeMember extends  React.Component {
    render() {
        return (
            <div>
                <div>Authenticated</div>
                <EditableText assetid="home.main.title" >Editable ttext</EditableText>
            </div>
        )
    }
}


class Login extends  React.Component {
    render() {
        return (
            <div>
                <LoginForm />
            </div>
        )
    }
}



@observer
class App extends React.Component {

    @observable initialized = false

    componentDidMount = async () => {
        if (GlobalConfig.gaAccountId) {
            ReactGA.initialize(GlobalConfig.gaAccountId, {debug: false})
            ReactGA.set({userId:GlobalConfig.userId});
            ReactGA.pageview(window.location.pathname)
        }

        await account.get(true)
        assetManager.initialize()
        this.initialized = true
    }

    render() {
        if (!this.initialized)
            return <Loader loaded={false} />
        return (
            <Router history={history} >
                <div>
                    <ScrollToTopWithRouter/>
                    <PageViewWithRouter/>
                    <CookieBanner
                        message="Yes, we also use cookies to personalise content. You consent to our cookies if you continue to use our website."
                        onAccept={() => {}}
                        cookie="user-has-accepted-cookies"
                    />
                    <Switch>
                        <Route exact path="/" render={(props) => { return (
                            account.isLoggedIn ?
                                    <HomeMember {...props} />
                                    :
                                    <Home {...props} />
                        ) } }/>
                        <Route path="/members-only" render={(props) => account.isLoggedIn ?
                                    <HomeMember {...props} />
                                    :
                                    <Login {...props}  next="//members-only" />} />
                    </Switch>
                </div>
            </Router>
        )
    }
}

export default hot(module)(App)
