import React from 'react'
import { render } from "react-dom"
import App from "./app"

const root = document.getElementById('MainApp');

const render_app = () => {
    render(<App {...(root.dataset)} />, root)
};

__webpack_public_path__ = __webpack_public_path__ || GlobalConfig.staticUrl+'bundles/prod/';

if (
  'fetch' in window &&
  'Intl' in window &&
  'URL' in window &&
  'Map' in window &&
  'forEach' in NodeList.prototype &&
  'startsWith' in String.prototype &&
  'endsWith' in String.prototype &&
  'includes' in String.prototype &&
  'includes' in Array.prototype &&
  'assign' in Object &&
  'entries' in Object &&
  'keys' in Object
) {
    render_app();
} else {
  import('@babel/polyfill').then(render_app);
}