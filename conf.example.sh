#!/bin/bash

# the name of your main django app (this is what you add to INSTALLED_APPS
NAME=djangoapp

# where settings.py is without the .settings at the end
DJANGO_SETTINGS_ROOT=${NAME}

# the root where ripple-web nis cloned usually this directory of this conf.sh file
RIPPLE_SRC_HOME=path/to/your/project

# path to ripple-react source
RIPPLE_REACT=/path/to/ripple-react

# path to ripple-django
RIPPLE_DJANGO=/path/to/ripple-django

# path to an already configured installed Virtualenv
VENV=/path/to/virtualenv

# path to an already configured installed Noedenv
NENV=/path/to/nodeenv

# where to map the backend server
BACKEND_HOST="localhost"
BACKEND_PORT="8000"

# where to map the frontend server
FRONTEND_HOST="localhost"
FRONTEND_PORT="3000"

# where to map the supervisord port (the host can not be changed here)
SUPERVISORD_PORT="9001"


# GCLOUD deployment settings
# !!!!!! must not be in GIT !!!!!!!

# staging gcloud project ID
STAGING_GC_PROJECT=

# staging gcloud bucket name
STAGING_GC_GS_NAME=

# staging gcloud database settings
STAGING_DB_NAME=
STAGING_DB_USER=
STAGING_DB_PASSWORD=
STAGING_DB_INSTANCE=


# same for the LIVE config
LIVE_GC_PROJECT=
LIVE_GC_GS_NAME=

LIVE_DB_NAME=
LIVE_DB_USER=
LIVE_DB_PASSWORD=
LIVE_DB_INSTANCE=