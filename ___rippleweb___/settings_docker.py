from .settings import *
import os

# !!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# settings.py and LOCAL_SETTINGS.py have already been imported
# some configuration has already been set based on some config options like DEBUG
# or USE_WEBPACK_LOADER
# These settings probably have to be set again here (like USE_WEBPACK_LOADER)
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# override settings that are already set based on LOCAL_SETTINGS or DEFAULT

if not USE_WEBPACK_LOADER:
    INSTALLED_APPS.append('webpack_loader')
    WEBPACK_LOADER = {
        'DEFAULT': {
            'BUNDLE_DIR_NAME': 'bundles/local/',  # end with slash
            'STATS_FILE': os.path.join(BASE_DIR, './webpack-stats-local.json'),
        },
    }

USE_WEBPACK_LOADER = True

# override defaults for running in docker dev here

GA_ACCOUNT_ID = 'UA-120451957-2'
GMAPS_API_KEY = 'AIzaSyDX82bPEtAhwLU5xYPD_jqJ5t0MwMqnZyM'
DEBUG = True
LOCAL_LOGGING = False,
ALLOW_ROBOTS = False
SITE_URL = 'http://localhost:8000'

INSTALLED_APPS.append('django_extensions')
