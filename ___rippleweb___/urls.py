"""rippleweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.views.generic.base import TemplateView
from django.conf import settings


class ContextTemplateView(TemplateView):

    context = {}

    def get_context_data(self, **kwargs):
        context = super(TemplateView, self).get_context_data(**kwargs)
        m = context.copy()
        m.update(self.context)
        return m

    def dispatch(self, request, *args, **kwargs):
        return super(TemplateView, self).dispatch(request, *args, **kwargs)


MainApp = 'main-app.html'

if settings.USE_WEBPACK_LOADER:
    MainApp = 'main-app_wpl.html'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', ContextTemplateView.as_view(template_name=MainApp, context={"title": "Ripple Framework Example App"}), name="root"),
    path('robots.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    path('api/', include('ripple.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
