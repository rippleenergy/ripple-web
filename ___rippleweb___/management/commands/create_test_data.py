from django.core.management.base import BaseCommand


from django.utils import timezone

from django.core.files.uploadedfile import SimpleUploadedFile
import os
from django.conf import settings
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    help = "Create Test Data in Database"

    def __init__(self):
        super().__init__()
        self.image_path = image_path = os.path.join(settings.BASE_DIR, 'media/test/')

    def add_arguments(self, parser):
        return
        parser.add_argument('sample', nargs='+')

    def create_image(self, name):

        type = name.split('.')[1]
        image = SimpleUploadedFile(name=name,
                                   content=open(os.path.join(self.image_path, name), 'rb').read(),
                                   content_type='image/'+type)
        return image

    def handle(self, *args, **options):

        now = timezone.now()

        try:
            User.objects.get(email="admin@example.com")
        except User.DoesNotExist:
            # if admin doesn't exist create it
            admin = User(email="admin@example.com",
                         is_staff=True, is_active=True,
                         is_superuser=True,
                         is_email_verified=True,
                         profile_pic=None,
                         date_joined=now)

            # not so secret password used for test admin
            admin.set_password('password')
            admin.save()

