#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

cd ..

if [[ ${DOCKER} ]]; then
    RUNSERVER="runserver_plus --nopin"
fi

if [[ ! -f ./data/db.sqlite3 ]]; then
   # create a new test database with test data
   CREATE_DB=True
   MIGRATE=True
fi

if [[ ${CREATE_DB} ]]; then
    # clear all previous migration instructions as we start fresh
    rm ${NAME}/migrations/0*
fi

if [[ $1 == "migrate" ]] || [[ ${MIGRATE} ]]; then
    echo "running migrations"
    python3 manage.py makemigrations --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
    python3 manage.py migrate --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
fi

if [[ ${CREATE_DB} ]]; then
  # create test data in the database including creating admin@rippleenergy.com
  echo "creating test data"
  python3 manage.py create_test_data --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
fi

# must be exec as the shell process must be replaced by this for supervisord to work correctly
exec python3 manage.py ${RUNSERVER:-runserver} ${BACKEND_HOST}:${BACKEND_PORT} --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"