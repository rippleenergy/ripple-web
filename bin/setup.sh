#!/bin/bash

# $1 base name

BASE_NAME=$1
BASE_NAME=${BASE_NAME:-___rippleweb___}

# this is our source bin
cd `dirname "$0"`

# this is this root checkout
cd ..

GITUSER=$(git config remote.origin.url | grep -oP "https://\K[^@]*")

SRC_HOME=`pwd`

cd ..

ROOT_DIR=`pwd`

git clone https://${GITUSER}@bitbucket.org/rippleenergy/ripple-react.git
git clone https://${GITUSER}@bitbucket.org/rippleenergy/ripple-docker.git
git clone https://${GITUSER}@bitbucket.org/rippleenergy/ripple-django.git

cd ${SRC_HOME}

mv ___rippleweb___ ${BASE_NAME}

mkdir static
mkdir ${BASE_NAME}
mkdir ${BASE_NAME}/static
mkdir data

sed -i "s/___rippleweb___/${BASE_NAME}/g"  main.py manage.py package.json package-lock.json ${BASE_NAME}/settings.py ${BASE_NAME}/wsgi.py

cd ..

cat <<EOF >ripple-docker/dev-all/conf.sh
# your docker account if you want to push into the cloud
IMAGE_NAMESPACE=${BASE_NAME}

# absolute path to your project source
RIPPLE_SRC_HOME=${SRC_HOME}
EOF


python3 -m venv ./venv
source ./venv/bin/activate

cd ${SRC_HOME}
pip install -r requirements.txt

cd ..

nodeenv nenv
source ./nenv/bin/activate

cd ${SRC_HOME}
npm install

cat <<EOF >${SRC_HOME}/conf.sh
# the name of your project
NAME=${BASE_NAME}

# the base module of settings, for ripple_one this is config (from config.settings)
DJANGO_SETTINGS_ROOT=${BASE_NAME}

# the absolute path of this directory
RIPPLE_SRC_HOME="${SRC_HOME}"

# the absolute path of the source root of ripple-react (it must be built! as the dist directory will
# be mapped)
RIPPLE_REACT="${ROOT_DIR}/ripple-react"

# the absolute path of the source root of ripple-django (this will not need to be built or configured)
RIPPLE_DJANGO="${ROOT_DIR}/ripple-django"

# mapping in the host or when run locally (in docker these are always 0.0.0.0:8000
BACKEND_HOST="localhost"
BACKEND_PORT="8000"

# mapping in the host or when run locally (in docker these are always 0.0.0.0:3000
FRONTEND_HOST="localhost"
FRONTEND_PORT="3000"

SUPERVISORD_PORT="9001"

# set up virtualenv and nodeenv to run locally (using requirements.txt for pip and package.json for npm)
VENV="${ROOT_DIR}/venv"
NENV="${ROOT_DIR}/nenv"

EOF


(cd ${ROOT_DIR}/ripple-react; npm install) || exit 1

${ROOT_DIR}/ripple-docker/dev-all/build_image.sh || exit 1
${ROOT_DIR}/ripple-docker/dev-all/create_container.sh || exit 1

docker exec ${BASE_NAME} build_db.sh
docker exec ${BASE_NAME} migrate.sh
docker attach ${BASE_NAME}
