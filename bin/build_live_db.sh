#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

cd ..

SETTINGS=${DJANGO_SETTINGS}_live_DB

read -p "This will kill the live database. Are you sure you want to continue?" -n 1 -r
echo    # move to a new line

if [[ $REPLY =~ ^[Yy]$ ]]
then

    read -p "The current live database must be archived, this script will not do this. Are you sure you want to continue?" -n 1 -r
    echo    # move to a new line

    if [[ $REPLY =~ ^[Yy]$ ]]
    then

        echo "Edit the script to run... ;) "
        exit

        # create a backup anyway
        mysqldump -h 127.0.0.1 -u ${LIVE_DB_USER} --password=${LIVE_DB_PASSWORD}  ${LIVE_DB_NAME} > live_dump_tmp.sql

        mysql -h 127.0.0.1 -u ${LIVE_DB_USER} --password=${LIVE_DB_PASSWORD} -B -e "DROP DATABASE ${LIVE_DB_NAME};"
        mysql -h 127.0.0.1 -u ${LIVE_DB_USER} --password=${LIVE_DB_PASSWORD} -B -e "CREATE DATABASE ${LIVE_DB_NAME};"

        python3 manage.py makemigrations --settings=${SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
        python3 manage.py migrate --settings=${SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
    fi
fi