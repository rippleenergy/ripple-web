#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

./cloud_sql_proxy -instances="${LIVE_DB_INSTANCE}" &

CSPPID=$!

cd ..

sleep 3

mysqldump -h 127.0.0.1 -u "${LIVE_DB_USER}" --password="${LIVE_DB_PASSWORD}"  ${LIVE_DB_NAME} > live_dump_`date +%y%m%d-%H%M%S`.sql

kill ${CSPPID}