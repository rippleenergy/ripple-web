#!/usr/bin/env bash

cd `dirname "$0"`
source ./conf.sh

cd ..

python3 manage.py makemigrations --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
python3 manage.py migrate --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"