#!/usr/bin/env bash

cd `dirname "$0"`
source ./conf.sh

cd ..

set -e

if [[ -f ./db.sqlite3 ]]; then
    rm ./db.sqlite3
fi

rm -f ${NAME}/migrations/0*
python3 manage.py makemigrations --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
python3 manage.py migrate --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
python3 manage.py create_test_data --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"

