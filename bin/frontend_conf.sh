#!/bin/bash

source ./conf.sh

cd ..

if [[ ! ${DOCKER} ]]; then
    if [[ ! -d ./node_modules/ripple-react ]]; then
        mkdir ./node_modules/ripple-react
    fi

    if [[ ! -d ./node_modules/ripple-react/dist ]]; then
        ln -s ${RIPPLE_REACT}/dist ./node_modules/ripple-react/dist
    fi

    if [[ ! -f ./node_modules/ripple-react/package.json ]]; then
        ln -s ${RIPPLE_REACT}/package.json ./node_modules/ripple-react/package.json
    fi
fi