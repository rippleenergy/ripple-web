#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

./cloud_sql_proxy -instances="${STAGING_DB_INSTANCE}" &

CSPPID=$!

cd ..

sleep 3

SETTINGS=${DJANGO_SETTINGS}_staging_DB

mysql -h 127.0.0.1 -u ${STAGING_DB_USER} --password=${STAGING_DB_PASSWORD} -B -e "DROP DATABASE ${STAGING_DB_NAME};"
mysql -h 127.0.0.1 -u ${STAGING_DB_USER} --password=${STAGING_DB_PASSWORD} -B -e "CREATE DATABASE ${STAGING_DB_NAME};"

python3 manage.py makemigrations --settings=${SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
python3 manage.py migrate --settings=${SETTINGS} --pythonpath="${RIPPLE_DJANGO}"
python3 manage.py create_test_data --settings=${SETTINGS} --pythonpath="${RIPPLE_DJANGO}"

kill ${CSPPID}