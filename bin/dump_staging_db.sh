#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

./cloud_sql_proxy -instances="${STAGING_DB_INSTANCE}" &

CSPPID=$!

cd ..

sleep 3

mysqldump -h 127.0.0.1 -u ${STAGING_DB_USER} --password=${STAGING_DB_PASSWORD}  ${STAGING_DB_NAME} > staging_dump_`date +%y%m%d-%H%M%S`.sql

kill ${CSPPID}