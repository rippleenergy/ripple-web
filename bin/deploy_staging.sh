#!/bin/bash

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# Script to Deploy to the Google Cloud (Flexible or Standard Environment)
# gcloud must be installed with security credentials configured
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


cd `dirname "$0"`
source ./frontend_conf.sh

if [[  "${STAGING_GC_PROJECT}" == "" ]] || [[  "${STAGING_GC_GS_NAME}" == "" ]]; then
    echo "APP Engine project configuration is missing!!"
    exit
fi

read -p "Deploying to ${STAGING_GC_PROJECT} . Are you sure? " -n 1 -r
echo    # (optional) move to a new line

if [[ $REPLY =~ ^[Yy]$ ]]
then

    rm -r -f -d static/bundles
    npm run build-prod

    cp -r ${RIPPLE_DJANGO}/ripple ripple

    python manage.py collectstatic --no-input --settings=${DJANGO_SETTINGS}

    gsutil -m rsync -R static/ gs://${STAGING_GC_GS_NAME}/static/
    gcloud app deploy --project ${STAGING_GC_PROJECT} --quiet

    rm -r -f ripple
fi