#!/bin/bash

source ../conf.sh

if [ -f /home/conf ]; then
# running in the ripple-docker container
    DJANGO_SETTINGS=${DJANGO_SETTINGS_ROOT}.settings_docker
    RIPPLE_SRC_HOME=/home/src
    RIPPLE_REACT=/home/ripple-react
    RIPPLE_DJANGO=/home/ripple-django
    DOCKER=True
    BACKEND_PORT=8000
    BACKEND_HOST="0.0.0.0"
    FRONTEND_PORT=3000
    FRONTEND_HOST="0.0.0.0"
else
    DJANGO_SETTINGS=${DJANGO_SETTINGS_ROOT}.settings
    if [[ "${VIRTUAL_ENV}" == "" ]] || [[ "${NODE_VIRTUAL_ENV}" == "" ]]; then
        source ${VENV}/bin/activate
        source ${NENV}/bin/activate
    fi
fi