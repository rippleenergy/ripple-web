#!/usr/bin/env bash

cd `dirname "$0"`
source ./conf.sh

cd ..

python3 manage.py create_test_data --settings=${DJANGO_SETTINGS} --pythonpath="${RIPPLE_DJANGO}"

