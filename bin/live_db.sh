#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

cd ..

mysql -h 127.0.0.1 -u "${LIVE_DB_USER}" --password="${LIVE_DB_PASSWORD}" ${LIVE_DB_NAME} "$@"

# for example customer registrations:
# ./bin/live_db.sh -B -e "select * from api_usercontact" > cutomers.txt
