#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

cd ..

mysqldump -h 127.0.0.1 -u "${STAGING_DB_USER}" --password="${STAGING_DB_PASSWORD}" ${STAGING_DB_NAME} "$@"

