#!/usr/bin/env bash

# wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
# chmod +x cloud_sql_proxy

cd `dirname "$0"`
source ./conf.sh

./cloud_sql_proxy -instances="${STAGING_DB_INSTANCE}"