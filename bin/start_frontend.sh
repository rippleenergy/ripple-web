#!/bin/bash

cd `dirname "$0"`
source ./frontend_conf.sh

#current dir is now the frontend root

# must be exec as the shell process must be replaced by this for supervisord to work correctly
exec ./node_modules/.bin/webpack-dev-server --env.port=${FRONTEND_PORT} --env.host=${FRONTEND_HOST}
