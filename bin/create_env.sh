#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

cd ..

if [[ ${DOCKER} ]]; then
    echo "Don't run this in docker!"
    exit
fi

if [[ "${VIRTUAL_ENV}" == "" ]] || [[ "${NODE_VIRTUAL_ENV}" == "" ]]; then
    echo "Virtualenv or Nodenv already detected."
    exit
fi

python3 -m venv ${VENV}
source ${VENV}/bin/activate
pip install -r requirements.txt

nodeenv ${NENV}
source ${NENV}/bin/activate
npm install