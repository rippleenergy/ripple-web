#!/bin/bash

cd `dirname "$0"`
source ./conf.sh

./cloud_sql_proxy -instances="${STAGING_DB_INSTANCE}" &

CSPPID=$!

cd ..

sleep 3

SETTINGS=${DJANGO_SETTINGS}_staging_DB


python3 manage.py shell --settings=${SETTINGS} --pythonpath="${RIPPLE_DJANGO}"

kill ${CSPPID}