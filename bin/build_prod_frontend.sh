#!/bin/bash

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# Script to Deploy to the Google Cloud (Flexible or Standard Environment)
# gcloud must be installed with security credentials configured
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


cd `dirname "$0"`
source ./frontend_conf.sh


rm -r -f -d static/bundles
npm run build-prod